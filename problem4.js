let inventory = require("./js_drill_2");
const correctSalaries = require("./problem3");

function sumOfSalaries(){
    let newInventory = correctSalaries();
    if(!Array.isArray(inventory)){
        throw new Error('Invalid inventory Found');
    }
    let sum=0;
    for(let id = 0; id < newInventory.length ; id++ ){
        sum+=newInventory[id].corrected_salary;
    }
    return sum;

}
module.exports=sumOfSalaries;