const inventory = require('../js_drill_2');
const findWebDevelopers= require('../problem1');

try {
    const webDeveloper = findWebDevelopers(inventory);
    console.log(webDeveloper);
} catch (error) {
    console.error(error.message);
}